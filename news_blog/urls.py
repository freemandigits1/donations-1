from django.conf.urls import include, url

from .views import (
	news_list,
	news_detail,)

urlpatterns = [
    url(r'^$', news_list, name="newslist"),
    url(r'^(?P<slug>[\w-]+)/$', news_detail, name="newsdetail"),
    # url(r'^contact/$', contact, name="contact"),
   
]