from django.shortcuts import render

from meetup.models import Meetup


def meetup_list(request):
    items = Meetup.objects.filter(active=True)
    return render(request, 'meetup/meetups.html', {'items': items})
