from django.shortcuts import render
from .models import Volunteer
# Create your views here.


def volunteer(request):
	volunteers = Volunteer.objects.all()
	return render(request, 'volunteer/volunteer.html', {"volunteers": volunteers})